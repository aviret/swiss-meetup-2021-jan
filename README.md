# 1. Swiss Meetup 2021 January

"First time GitLab and CI/CD"

## Resources

- [Event](https://www.meetup.com/switzerland-gitlab-meetup-group/events/274665622/)
- [Slides](https://docs.google.com/presentation/d/1exhtIwOa9weC48G9V5KQOVDn-OEKcDSrvVN8gek1j78/edit?usp=sharing)
- [Organisation](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4015)

The slides provide the step-by-step instructions as exercises for this repository.

- CI/CD Getting Started
- Security scanning
